var firebase = require("firebase");

module.exports = function(req,res,next){
  firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    next();
  } else {
    res.redirect('/');
  }
});
}
