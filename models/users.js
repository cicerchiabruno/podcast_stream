var mongoose = require('mongoose');
Schema = mongoose.Schema;

mongoose.Promise = global.Promise;
var promise = mongoose.connect('mongodb://127.0.0.1:27017', {
  useMongoClient: true,
});

var email_match = [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, "El email es invalido"];

var user_schema = new Schema({
  name: {
    type: String,
    required: true,
    maxlength: [50, "Nombre de usuario muy largo"]
  },
  email: {
    type: String,
    required: true,
    match: email_match,
  },
  password: {
    type: String,
    required: true,
    minlength: [8, "Ingresa 8 caracteres como minimo"],
    maxlength: [50, "Contraseña demasiado larga"]
  }
});

var User = mongoose.model("User", user_schema);
module.exports.User = User;
