const express = require('express');
const app = express();
var bodyParser = require('body-parser');
var jade = require('jade');
var path = require('path');
var firebase = require("firebase");
var sleep = require('sleep');
var providerGoogle = new firebase.auth.GoogleAuthProvider();
var providerFacebook = new firebase.auth.FacebookAuthProvider();
providerFacebook.addScope('user_birthday');

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDKV2qt9bnzDcgPsqqum-smQ1oMXj5vMkU",
    authDomain: "linecode-7c1b3.firebaseapp.com",
    databaseURL: "https://linecode-7c1b3.firebaseio.com",
    projectId: "linecode-7c1b3",
    storageBucket: "linecode-7c1b3.appspot.com",
    messagingSenderId: "162889000553"
};
firebase.initializeApp(config);
var database = firebase.database();

//Use Start

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static("static"));
app.set("view engine", "jade");

//Seleccion de puerto
var puerto = (process.env.PORT || 8080);
app.listen(puerto, function () {
  console.log('Aplicacion corriendo en el puerto ' + puerto +"!");
})

// Directorios Start Paginas
app.get('/', index);
app.get('/app', appp);
app.get('/new_config');

function index (req, res) {
  console.log('index');
  var user = firebase.auth().currentUser;
  if (user) {
    res.redirect('/app');
} else {
    res.render('home');
  }
}

function appp (req, res) {
  var user = firebase.auth().currentUser;
  var uid;
  if (user != null) {
    uid = user.displayName;
  }
  console.log(uid);
  if (user) {
    res.render('app/app')
} else {
    res.redirect('/login')
  }
}

// Login Pages

app.get('/login', login);
app.get('/login_user', loginuserget);
app.get('/logout', logout);
app.get('/facebook_login', loginfacebook);


function login (req, res) {
  var user = firebase.auth().currentUser;
  console.log('login');
  if (user) {
    res.redirect('/app');
} else {
    res.render('login');
  }
}

function logout (req, res) {
  firebase.auth().signOut().then(function() {
    res.redirect('/');
}, function(error) {
   res.redirect('/login');
});
}

function loginuserget (req, res) {
  res.redirect('/app');
}
function loginfacebook (req, res, next) {
  firebase.auth().signInWithPopup(providerFacebook).then(function(result) {
  // This gives you a Facebook Access Token. You can use it to access the Facebook API.
  var token = result.credential.accessToken;
  // The signed-in user info.
  var username = result.user;
  // ...
}).catch(function(error) {
  // Handle Errors here.
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log(errorMessage);
  // The email of the user's account used.
  var email = error.email;
  // The firebase.auth.AuthCredential type that was used.
  var credential = error.credential;
  // ...
});

}

function register (req, res) {
  firebase.auth().createUserWithEmailAndPassword(req.body.email, req.body.password).catch(function(error) {
  var errorCode = error.code;
  var errorMessage = error.message;
  console.log(errorMessage);
});
  res.sedirect('/new_config');
}

function new_userpost (req, res) {
  function writeUserData(username, name, last_name, imageUrl) {
  firebase.database().ref('users/' + uid).set({
    username: username,
    name: name,
    last_name: last_name,
    profile_picture : imageUrl
  });
}
}
