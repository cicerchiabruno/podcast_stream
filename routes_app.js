const express = require('express');
var router = express.Router();
var firebase = require("firebase");

router.get("/", function (req, res){
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      res.render("app/index.jade");
    }
    else {
      res.redirect("/");
  }
})

});

router.get("/category", function (req, res){
  res.sendfile("views/app/category/category.html")
});

router.get("/category/education", function (req, res){
  res.sendfile("views/app/category/education/education.html")
});

router.get("/category/music", function (req, res){
  res.sendfile("views/app/category/music/music.html")
});

router.get("/category/tecnology", function (req, res){
  res.sendfile("views/app/category/tecnology/tecnology.html")
});

module.exports = router;
